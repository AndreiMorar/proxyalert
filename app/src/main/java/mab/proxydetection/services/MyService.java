package mab.proxydetection.services;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.IOException;

import mab.proxydetection.R;
import mab.proxydetection.receivers.BroadcastReceiverService;
import mab.proxydetection.ui.AMain;

public class MyService extends Service {

    //    private final int SHOW_DIALOG_DELAY_SHORT = 10 * 60 * 1000;//10 min
    //    private final int SHOW_DIALOG_DELAY_LONG = 60 * 60 * 1000;//10 sec

    private final int SHOW_DIALOG_DELAY_SHORT = 3 * 1000;
    private final int SHOW_DIALOG_DELAY_LONG = 30 * 1000;

    private final boolean IS_ICS_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;

    private final int NOTIFICATION_ID = 1;

    private RemoteViews mNotificationViewNormal;

    private NotificationCompat.Builder mBuilder;

    private String mProxyAddress;
    private int mProxyPort = -1;

    private boolean mUseLongDelayNextTime = false;

    private BroadcastReceiverService mReceiver = new BroadcastReceiverService() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);

            String action = intent.getAction();

            if (ACTION_WIFI_STATE_CHANGED.equalsIgnoreCase(action)) {
                getProxyDetails();
                updateNotificationView();
            } else if (ACTION_PHONE_SCREEN_ON.equalsIgnoreCase(action)) {
            } else if (ACTION_PHONE_SCREEN_OFF.equalsIgnoreCase(action)) {
            } else if (ACTION_USE_LONG_DELAY_NEXT_TIME.equalsIgnoreCase(action)) {
                getProxyDetails();
                stopDialogDisplay();
                mUseLongDelayNextTime = true;
                startDialogDisplay(true);
            }


        }
    };

    private Handler mTrackDataHandler = new Handler();
    private Runnable mTrackDataRunnable = new Runnable() {
        @Override
        public void run() {
            showDialog();
        }
    };

    //========================================================================
    //========================================================================

    //------------------------------------------------------------------------
    //-------------------------------------------------------- Service (start)
    //------------------------------------------------------------------------

    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("MyService.onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("MyService.onStartCommand");
        getProxyDetails();

        startDialogDisplay(true);

        updateNotificationView();

        registerBroadcastReceiver();

        return START_STICKY;
    }

    //----------------------------------------------------------------------
    //-------------------------------------------------------- Service (end)
    //----------------------------------------------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcastReceiver();
    }


    private void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BroadcastReceiverService.ACTION_STATE_CHANGED);
        intentFilter.addAction(BroadcastReceiverService.ACTION_WIFI_STATE_CHANGED);
        intentFilter.addAction(BroadcastReceiverService.ACTION_PHONE_SCREEN_ON);
        intentFilter.addAction(BroadcastReceiverService.ACTION_PHONE_SCREEN_OFF);
        intentFilter.addAction(BroadcastReceiverService.ACTION_USE_LONG_DELAY_NEXT_TIME);
        registerReceiver(mReceiver, intentFilter);
    }

    private void unregisterBroadcastReceiver() {
        if (mReceiver != null) {
            try {
                unregisterReceiver(mReceiver);
            } catch (IllegalArgumentException e) {
                //*meh*
            }
        }
    }

    private void updateNotificationView() {
        System.out.println("MyService.updateNotificationView 1/2");
        printStackTrace();
        Context context = getApplicationContext();

        if (mBuilder == null || mNotificationViewNormal == null) { // CREATE FROM SCRATCH
            System.out.println("MyService.updateNotificationView 2/2");

            //Setup the notification builder
            mBuilder = new NotificationCompat.Builder(context)
                    .setAutoCancel(false) //Don't cancel the notification when it's clicked
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setOngoing(true);

            //Define and set the content of the notification
            mNotificationViewNormal = new RemoteViews(context.getPackageName(), R.layout.notification_normal);
        }

        //Set appropriate data to the view
        setDataToNotificationView(mBuilder, mNotificationViewNormal);
    }

    private void setDataToNotificationView(final NotificationCompat.Builder builder, final RemoteViews normalView) {
        System.out.println("MyService.setDataToNotificationView 1/2");
        if (normalView != null) {
            System.out.println("MyService.setDataToNotificationView 2/2");

            /*
             * NORMAL
             */
            if (proxyExists()) {
                normalView.setTextViewText(R.id.message, "Connected through: \n" + mProxyAddress + ":" + mProxyPort);
            } else {
                normalView.setTextViewText(R.id.message, "No proxy detected...");
            }
        }

        buildNotificationAndStart(builder, normalView);
    }

    private void buildNotificationAndStart(NotificationCompat.Builder builder, RemoteViews normalView) {
        System.out.println("MyService.buildNotificationAndStart1/2");
        if (normalView != null) {
            System.out.println("MyService.buildNotificationAndStart 2/2");

            builder.setContent(normalView);

            Notification notification = builder.build();

            notification.contentView = normalView;

            //BIG VIEWS are only supported starting from API 16
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                //                notification.bigContentView = expandedView;
            }

            //Used for a medium sized notification view
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notification.headsUpContentView = normalView;
            }

            //            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            //            nm.notify(NOTIFICATION_ID, notification);
            startForeground(NOTIFICATION_ID, notification);
        }
    }

    private void getProxyDetails() {
        if (IS_ICS_OR_LATER) {
            mProxyAddress = System.getProperty("http.proxyHost");

            String portStr = System.getProperty("http.proxyPort");
            mProxyPort = Integer.parseInt((portStr != null ? portStr : "-1"));
        } else {
            mProxyAddress = android.net.Proxy.getHost(getApplicationContext());
            mProxyPort = android.net.Proxy.getPort(getApplicationContext());
        }
    }

    private boolean proxyExists() {
        return mProxyPort > 0 && !mProxyAddress.isEmpty();
    }

    private boolean pingFucker(String address) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 " + address);
            int mExitValue = mIpAddrProcess.waitFor();
            System.out.println(" mExitValue " + mExitValue);
            if (mExitValue == 0) {
                return true;
            } else {
                return false;
            }
        } catch (InterruptedException ignore) {
            ignore.printStackTrace();
            System.out.println(" Exception:" + ignore);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(" Exception:" + e);
        }
        return false;
    }

    private void startDialogDisplay(boolean useDelay) {
        getProxyDetails();

        if (mTrackDataHandler != null && mTrackDataRunnable != null) {
            mTrackDataHandler.removeCallbacks(mTrackDataRunnable);//just in case
            mTrackDataHandler.postDelayed(mTrackDataRunnable, useDelay ? (mUseLongDelayNextTime ? SHOW_DIALOG_DELAY_LONG : SHOW_DIALOG_DELAY_SHORT) : 0);

            mUseLongDelayNextTime = false;
        }
    }

    private void stopDialogDisplay() {
        mTrackDataHandler.removeCallbacks(mTrackDataRunnable);
    }

    protected void showDialog() {
        getProxyDetails();
        updateNotificationView();
        if (proxyExists()) {
            Intent i = new Intent(getApplicationContext(), AMain.class);
            i.putExtra(AMain.BUNDLE_SHOW_DIALOG, true);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplication().startActivity(i);

        }

        startDialogDisplay(true);
    }

    public static void printStackTrace() {
        System.out.println(Log.getStackTraceString(new Exception()));
    }
}