package mab.proxydetection.ui;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;

import mab.proxydetection.R;
import mab.proxydetection.receivers.BroadcastReceiverService;
import mab.proxydetection.services.MyService;

/**
 * @author MAB
 */
public class AMain extends Activity {

    /** Should be a boolean */
    public static final String BUNDLE_SHOW_DIALOG = "show_dialog";

    private MaterialDialog mDialog;

    private boolean mShowDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.a_main);

        Bundle b = getIntent().getExtras();
        if (b != null && b.containsKey(BUNDLE_SHOW_DIALOG)) {
            mShowDialog = b.getBoolean(BUNDLE_SHOW_DIALOG);
        }

        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mShowDialog) {
            finish();
        }
    }

    private void init() {
        if (mShowDialog) {
            showDialog();
        } else {
            Intent intent = new Intent(this, MyService.class);

            System.out.println("AMain.onCreate 1/2");
            if (!isServiceRunning(this, MyService.class.getName())) {
                System.out.println("AMain.onCreate 2/2");
                startService(intent);
            }
        }
    }

    public static boolean isServiceRunning(Context ctx, String serviceClassName) {
        if (ctx != null && serviceClassName != null) {
            ActivityManager manager = (ActivityManager) ctx.getSystemService(ctx.ACTIVITY_SERVICE);
            if (manager != null) {
                for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                    if (serviceClassName.equals(service.service.getClassName())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected void showDialog() {
        if (mDialog == null || mDialog.isCancelled()) {
            mDialog = new MaterialDialog.Builder(this)
                    .title("Proxy alert !")
                    .content("*Psst* ... 'wondering why your app's not making that WS call ?\nYou're connected through a proxy right now !")
                    .cancelable(true)
                    .positiveText("Duuh !! Thanks !")
                    .positiveColor(Color.parseColor("#269026"))
                    .neutralText("I know. Remind me in 1 hour")
                    .neutralColor(Color.parseColor("#3d49e2"))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            AMain.this.finish();
                        }

                        @Override
                        public void onNeutral(MaterialDialog dialog) {
                            Intent i = new Intent();
                            i.setAction(BroadcastReceiverService.ACTION_USE_LONG_DELAY_NEXT_TIME);
                            AMain.this.sendBroadcast(i);
                            AMain.this.finish();
                        }
                    })
                    .cancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            AMain.this.finish();
                        }
                    })
                    .show();
        }
    }

    protected void hideDialog() {
        if (mDialog != null && !mDialog.isCancelled()) {
            mDialog.cancel();
        }
    }
}
