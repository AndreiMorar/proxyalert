package mab.proxydetection.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mab.proxydetection.services.MyService;

public class AutostartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, MyService.class);
        context.startService(startServiceIntent);
    }
}