package mab.proxydetection.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BroadcastReceiverService extends BroadcastReceiver {

    public static final String ACTION_WIFI_STATE_CHANGED = "android.net.wifi.WIFI_STATE_CHANGED";
    public static final String ACTION_STATE_CHANGED = "android.net.wifi.STATE_CHANGE";
    public static final String ACTION_PHONE_SCREEN_ON = Intent.ACTION_SCREEN_ON;
    public static final String ACTION_PHONE_SCREEN_OFF = Intent.ACTION_SCREEN_OFF;
    public static final String ACTION_USE_LONG_DELAY_NEXT_TIME = "mab.proxydetection.USE_LONG_DELAY";

    @Override
    public void onReceive(Context context, Intent intent) {
    }
}